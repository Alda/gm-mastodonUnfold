This is a small script to add a button that unfold every CW on a conversation page of Mastodon.

It is not intended to work on the webclient but on the public toot pages.

You need a browser extension like ViolentMonkey for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/) or [Chromium](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?utm_source=chrome-ntp-icon) that allows you to execute user scripts.

Just click here to install it : https://framagit.org/Alda/gm-mastodonUnfold/raw/master/mastodonUnfold.user.js