// ==UserScript==
// @name        Mastodon Unfold All
// @description Creates a button that unfold every Content Warning on the page
// @namespace   mastoUnfold
// @version     1.1
// @grant       none
// @include     https://*
// @noframes
// ==/UserScript==

let poweredBy = document.getElementsByClassName('powered-by')[0]
let poweredByA = poweredBy && poweredBy.getElementsByTagName('a')[0]
let isMastodon = poweredByA && poweredByA.innerText === "Mastodon"
if (!isMastodon) {
  throw 'Not a Mastodon page'
}

let unfoldButton = document.createElement('button')
unfoldButton.innerHTML = 'Unfold all'
unfoldButton.onclick = function () {
  let readMore = document.getElementsByClassName('status__content__spoiler-link')

  Array.prototype.forEach.call(readMore, function (link) {
    let clickEvt = document.createEvent('MouseEvents')
    clickEvt.initEvent('click', true, true)
    link.dispatchEvent(clickEvt)
  })
}

unfoldButton.style.position = 'fixed'
unfoldButton.style.top = '15px'
unfoldButton.style.left = '20px'

document.body.prepend(unfoldButton)
